# My Favourite Websites #

Using HTML to create a webpage with hyperlinks and links to resources.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open favourites.html in Sublime and have a look at its markup, notice the example favourites.
2. Create a list of your favourite websites and divide them into categories.
3. Use the example as a template to create your category lists.
4. If you need to and want to try, use the style.css to add styling to the page.
5. Use your knowledge of laying out content to make your work more visually appealing.
6. You can see your progress by typing in terminal 'open favourites.html'.